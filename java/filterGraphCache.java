import java.io.*;
import java.util.*;
import java.util.zip.*;
import java.util.concurrent.ExecutorService;  
import java.util.concurrent.Executors; 

public class filterGraphCache{
	
	public static void main(String [] args){
		
		try{
			ObjectInputStream ois=new ObjectInputStream(new BufferedInputStream(new FileInputStream(args[0]+".txtobj")));
			ArrayList<int []> tmp=(ArrayList<int []>)ois.readObject();
			String cache=args[1];
			ObjectInputStream fids=new ObjectInputStream(new BufferedInputStream(new GZIPInputStream(new FileInputStream("./cache/core_"+cache+"_1.ids"))));
			
			ArrayList<Integer> ids=(ArrayList<Integer>) fids.readObject();
			
			
			Set set = new HashSet(ids);
			for (int i=0;i<tmp.size();i++){
				if(i%100000==0) {System.out.println(i);}
				if(!set.contains((Integer)i)){
					tmp.set(i,null);
				}
			}
			ObjectOutputStream oos=new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream("./"+args[0]+"_"+cache+".obj")));
			oos.writeObject(tmp);
			oos.flush();
			oos.close();
			
		}
		catch(Exception e){
			
			e.printStackTrace();
		}
	}
	
}