import java.io.*;
import java.util.*;
import java.util.zip.*;
import java.util.concurrent.ExecutorService;  
import java.util.concurrent.Executors;  
public class lastCores2{
	public ArrayList<int []> web=new ArrayList<int []>();
	public ArrayList<Integer> outD=new ArrayList<Integer>();
	public ArrayList<int []> webr=new ArrayList<int []>();
	public ArrayList<Integer> inD=new ArrayList<Integer>();
	public ArrayList<Integer> ids=new ArrayList<Integer>();
	//assume ids are numeric 0 to N no missing values in between
	
	
	public lastCores2(String out,String in) throws Exception{
		System.out.println("Reading out");
		ObjectInputStream ois=new ObjectInputStream(new BufferedInputStream(new FileInputStream(out),81920000));
		web=(ArrayList<int []>)ois.readObject();
		System.out.println("Reading in");
		ois=new ObjectInputStream(new BufferedInputStream(new FileInputStream(in),81920000));
		webr=(ArrayList<int []>)ois.readObject();
		System.out.println("Done");
		
	}
	public void loadCache(int CacheIn) throws Exception{
		System.out.println("Reading cache");
		//find closest cache
		File t = new File("./cache/core_"+CacheIn+"_1.ids");
		while(!t.exists()) { 
			CacheIn=CacheIn-1;
			t = new File("./cache/core_"+CacheIn+"_1.ids");
		}
		ObjectInputStream fids=new ObjectInputStream(new BufferedInputStream(new GZIPInputStream(new FileInputStream("./cache/core_"+CacheIn+"_1.ids"))));
		ObjectInputStream fid=new ObjectInputStream(new BufferedInputStream(new GZIPInputStream(new FileInputStream("./cache/core_"+CacheIn+"_1.ideg"))));
		ObjectInputStream fod=new ObjectInputStream(new BufferedInputStream(new GZIPInputStream(new FileInputStream("./cache/core_"+CacheIn+"_1.odeg"))));
		ids=(ArrayList<Integer>) fids.readObject();
		inD=(ArrayList<Integer>) fid.readObject();
		outD=(ArrayList<Integer>) fod.readObject();
		fids.close();
		fid.close();
		fod.close();
		System.out.println("Done");
		
	}
	
	
	
	public void compute(int k,int l){
		System.err.println("Starting k:"+k+" l:"+l);
		boolean ff=true;
		int o=0;
		while(ff){
			StringBuilder value = new StringBuilder("k:");
			value.append(k).append(" l:").append(l).append(" o: ").append(o);
			System.err.println(value);
			ff=false;
			ArrayList<Integer> tokeep=new ArrayList<Integer>();
			int temp=0;
			for(int i=0;i<ids.size();i++){
				
				if(i%100000==0){
					StringBuilder vs = new StringBuilder("\t\t k:");
					vs.append(k).append(" l:").append(l).append(" i: ").append(i);
					System.err.println(vs);
					
				}
				int id=ids.get(i);
				if(inD.get(id)>=k && outD.get(id)>=l){
					//ff=true;
					//ids.remove(i);
					//i--;
					tokeep.add(id);
				}
				else{
					for (int j=0;j<web.get(id).length;j++){
						temp=web.get(id)[j];
						inD.set(temp,inD.get(temp)-1);
					}
					for (int j=0;j<webr.get(id).length;j++){
						temp=webr.get(id)[j];
						outD.set(temp,outD.get(temp)-1);
					}
					ff=true;
				}
			}
			ids=tokeep;
			o++;
		}

	}
	public int size(){
		return this.ids.size();
	}
	public void saveIDS(String path)throws Exception{
		File file = new File(path);
		PrintWriter printWriter = new PrintWriter(file);
		for(int i=0;i<ids.size();i++){
		printWriter.println(ids.get(i));
		}
		printWriter.flush();
		printWriter.flush();
		Runtime.getRuntime().exec(new String[]{"bash","-c","gzip "+path});
	}
	public void Save_cache(ObjectOutputStream oi,ObjectOutputStream id,ObjectOutputStream od) throws Exception{
		oi.writeObject(ids);
		oi.flush();
		oi.close();
		id.writeObject(inD);
		id.flush();
		id.close();
		od.writeObject(outD);
		od.flush();
		od.close();
		
		
	}
	
	public static void main(String [] args){
		try{
			int from=Integer.parseInt(args[0]);
			int to=Integer.parseInt(args[1]);
			
			
			lastCores2 c=new lastCores2("./graphRESULT_"+args[2]+".obj","./graphRESULTR_"+args[2]+".obj");
			
			int out_=2;
			int in_=0;
			for (int i=from;i<to;i++){
				in_=i;
				System.gc();
				c.loadCache(in_);
				
				out_=2;
				File t = new File("./cache/core_"+in_+"_1.ids");
				if (!t.exists()){
					out_=1;
				}
				while (c.size()>0){
					c.compute(in_,out_);
					c.saveIDS("./cores_ids/core_"+in_+"_"+out_+".txt");
					out_++;
				}
				
			} 
			
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		

	}
	
  
}

	