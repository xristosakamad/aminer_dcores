import java.io.*;
import java.util.*;
import java.util.zip.*;
import java.util.concurrent.ExecutorService;  
import java.util.concurrent.Executors;  
public class lastCores{
	public ArrayList<int []> web=new ArrayList<int []>(140000000);
	public ArrayList<Integer> outD=new ArrayList<Integer>(140000000);
	public ArrayList<int []> webr=new ArrayList<int []>(140000000);
	public ArrayList<Integer> inD=new ArrayList<Integer>(140000000);
	public ArrayList<Integer> ids=new ArrayList<Integer>(140000000);
	//assume ids are numeric 0 to N no missing values in between
	
	public lastCores(String out,String in) throws Exception{
		
		int size=136220835;//hack it preallocation
		//another hack to preallocate
		int i=0;
		BufferedReader f=new BufferedReader(new FileReader("./degreesINOUT.txt"));
		String ln=f.readLine();
		while(ln!=null){
			if(i%100000==0){
				StringBuilder value = new StringBuilder("Loading ");
				value.append(i);
				System.out.println(value);}
			String [] lna=ln.split("\t");
			outD.add(Integer.parseInt(lna[0]));
			inD.add(Integer.parseInt(lna[1]));
			ids.add(i);
			ln=f.readLine();
			i++;
		}
		System.out.println("Reading out");
		ObjectInputStream ois=new ObjectInputStream(new BufferedInputStream(new FileInputStream(out),81920000));
		web=(ArrayList<int []>)ois.readObject();
		System.out.println("Reading in");
		ois=new ObjectInputStream(new BufferedInputStream(new FileInputStream(in),81920000));
		webr=(ArrayList<int []>)ois.readObject();
		System.out.println("Done");
		
	}
	public lastCores(String out,String in,boolean NoCache) throws Exception{
		
		int size=136220835;//hack it preallocation
		//another hack to preallocate
		int i=0;
		BufferedReader f=new BufferedReader(new FileReader("./degreesINOUT.txt"));
		String ln=f.readLine();
		while(ln!=null){
			if(i%100000==0){
				StringBuilder value = new StringBuilder("Loading ");
				value.append(i);
				System.out.println(value);}
			String [] lna=ln.split("\t");
			ln=f.readLine();
			i++;
		}
		System.out.println("Reading out");
		ObjectInputStream ois=new ObjectInputStream(new BufferedInputStream(new FileInputStream(out),81920000));
		web=(ArrayList<int []>)ois.readObject();
		System.out.println("Reading in");
		ois=new ObjectInputStream(new BufferedInputStream(new FileInputStream(in),81920000));
		webr=(ArrayList<int []>)ois.readObject();
		System.out.println("Done");
		
	}
	public lastCores(String out,String in,int CacheIn) throws Exception{
		
		int size=136220835;//hack it ...something something fuck it
		//another hack to preallocate
		int i=0;
		BufferedReader f=new BufferedReader(new FileReader("./degreesINOUT.txt"));
		String ln=f.readLine();
		while(ln!=null){
			if(i%100000==0){
				StringBuilder value = new StringBuilder("Loading ");
				value.append(i);
				System.out.println(value);}
			String [] lna=ln.split("\t");
			//outD.add(Integer.parseInt(lna[0]));
			//inD.add(Integer.parseInt(lna[1]));
			//ids.add(i);
			ln=f.readLine();
			i++;
		}
		System.out.println("Reading out");
		ObjectInputStream ois=new ObjectInputStream(new BufferedInputStream(new FileInputStream(out),81920000));
		web=(ArrayList<int []>)ois.readObject();
		System.out.println("Reading in");
		ois=new ObjectInputStream(new BufferedInputStream(new FileInputStream(in),81920000));
		webr=(ArrayList<int []>)ois.readObject();
		
		ois.close();
		System.out.println("Reading cache");
		//find closest cache
		File t = new File("./cache/core_"+CacheIn+"_1.ids");
		while(!t.exists()) { 
			CacheIn=CacheIn-1;
			t = new File("./cache/core_"+CacheIn+"_1.ids");
		}
		ObjectInputStream fids=new ObjectInputStream(new BufferedInputStream(new GZIPInputStream(new FileInputStream("./cache/core_"+CacheIn+"_1.ids"))));
		ObjectInputStream fid=new ObjectInputStream(new BufferedInputStream(new GZIPInputStream(new FileInputStream("./cache/core_"+CacheIn+"_1.ideg"))));
		ObjectInputStream fod=new ObjectInputStream(new BufferedInputStream(new GZIPInputStream(new FileInputStream("./cache/core_"+CacheIn+"_1.odeg"))));
		ids=(ArrayList<Integer>) fids.readObject();
		inD=(ArrayList<Integer>) fid.readObject();
		outD=(ArrayList<Integer>) fod.readObject();
		fids.close();
		fid.close();
		fod.close();
		System.out.println("Done");
		
		
	}
	
	
	
	public void compute(int k,int l){
		System.err.println("Starting k:"+k+" l:"+l);
		boolean ff=true;
		int o=0;
		while(ff){
			StringBuilder value = new StringBuilder("k:");
			value.append(k).append(" l:").append(l).append(" o: ").append(o);
			System.err.println(value);
			ff=false;
			ArrayList<Integer> tokeep=new ArrayList<Integer>();
			int temp=0;
			for(int i=0;i<ids.size();i++){
				
				if(i%100000==0){
					StringBuilder vs = new StringBuilder("\t\t k:");
					vs.append(k).append(" l:").append(l).append(" i: ").append(i);
					System.err.println(vs);
					
				}
				int id=ids.get(i);
				if(inD.get(id)>=k && outD.get(id)>=l){
					//ff=true;
					//ids.remove(i);
					//i--;
					tokeep.add(id);
				}
				else{
					for (int j=0;j<web.get(id).length;j++){
						temp=web.get(id)[j];
						inD.set(temp,inD.get(temp)-1);
					}
					for (int j=0;j<webr.get(id).length;j++){
						temp=webr.get(id)[j];
						outD.set(temp,outD.get(temp)-1);
					}
					ff=true;
				}
			}
			ids=tokeep;
			o++;
		}

	}
	public int size(){
		return this.ids.size();
	}
	public void saveIDS(String path)throws Exception{
		File file = new File(path);
		PrintWriter printWriter = new PrintWriter(file);
		for(int i=0;i<ids.size();i++){
		printWriter.println(ids.get(i));
		}
		printWriter.flush();
		printWriter.flush();
		Runtime.getRuntime().exec(new String[]{"bash","-c","gzip "+path});
	}
	public void Save_cache(ObjectOutputStream oi,ObjectOutputStream id,ObjectOutputStream od) throws Exception{
		oi.writeObject(ids);
		oi.flush();
		oi.close();
		id.writeObject(inD);
		id.flush();
		id.close();
		od.writeObject(outD);
		od.flush();
		od.close();
		
		
	}
	
	public static void main(String [] args){
		try{
			
			
			ArrayList<Integer> g=new ArrayList<Integer>();
			//first create the first lines ..we cant use cache for this
			int outT=1;
			int inT=1;
			lastCores c=new lastCores("./graphRESULT.txtobj","./graphRESULTR.txtobj");
			
			while (c.size()>0){
				c.compute(inT,0);
				c.saveIDS("./cores_ids/core_"+inT+"_"+0+".txt");
				inT++;
			}
			System.out.println("Done in");
			c=new lastCores("./graphRESULT.txtobj","./graphRESULTR.txtobj");
			
			while (c.size()>0){
				c.compute(0,outT);
				c.saveIDS("./cores_ids/core_"+0+"_"+outT+".txt");
				outT++;
			}
			System.out.println("Done out");
			
			int maxIN=inT;
			c=new lastCores("./graphRESULT.txtobj","./graphRESULTR.txtobj");
			inT=0;
			System.out.println("Finished all threads");
			for (inT=0;inT<maxIN;inT++){
				c.compute(inT,1);
				c.saveIDS("./cores_ids/core_"+inT+"_"+1+".txt");
				ObjectOutputStream oi=new ObjectOutputStream(new BufferedOutputStream(new GZIPOutputStream(new FileOutputStream("./cache/core_"+inT+"_"+1+".ids"))));
				ObjectOutputStream id=new ObjectOutputStream(new BufferedOutputStream(new GZIPOutputStream(new FileOutputStream("./cache/core_"+inT+"_"+1+".ideg"))));
				ObjectOutputStream od=new ObjectOutputStream(new BufferedOutputStream(new GZIPOutputStream(new FileOutputStream("./cache/core_"+inT+"_"+1+".odeg"))));
				c.Save_cache(oi,id,od);				
			} 
			
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		

	}
	
  
}

	