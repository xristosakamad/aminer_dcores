*June-July 2016*
# AMINER - D-CORE

This repository documents the process of processing the [AMINER](https://aminer.org/) dataset with the 
[D-Core](http://link.springer.com/article/10.1007/s10115-012-0539-0) decomposition.
The purpose of this analysis is two fold:
* Explore the D-CORE decomposition of the author to author citation graph.
* Provide evaluation metrics from the aforementioned exploration.

_On the rest of this document, for the D-core decomposition, we refer to  D-core(i,j) to the subgraph (resulting from the D-core algorithm) where every node has at least i IN-coming edges and at least j OUT-going edges_

###Author to Author Citation Graph:
Assuming Paper A with authors Ai cites Paper B with Authors Bi, then in the Author graph (Authors are the nodes) directed edges are create among all pairs of authors:
Ai->Bi

##Graph Size  & Related Work
The resulting graph has __140M__ nodes and __200M__ edges. The process and the code for converting the paper citation
graph to the author-citation graph is described later in this document. 

While the D-Core structure has not been explored in graphs of similar sizes, the simple version of k-core (undirected graphs) has has many different algorithms
for the exploration of very large undirected graphs. Following there is an indicative list of papers that have dealt with very large sized graphs using the *k-core* algorithm.
It is important to note that all of these papers try to provide essentially an alternative to the famous 
[Batagelj](http://vlado.fmf.uni-lj.si/pub/networks/doc/cores/cores.pdf). 
These algorithms are presented here in order to provide reference to importance of the scale of this D-Core computation:

* [K-Core Decomposition of Large Networks on a Single PC](http://www.vldb.org/pvldb/vol9/p13-khaouid.pdf) : While some of the graph sizes are even larger than this citation network, 
the maximum number of edges is only at 41M. 
* [Linear-Time Enumeration of Maximal k-Edge-Connected Subgraphs in Large Networks by Random Contraction](http://dl.acm.org/citation.cfm?id=2505751) :This work is on the "inverse" side as the number of edges is very large
but the networks themselves are much smaller in terms of vertices/nodes (max 7.5M).
* [Efficient Core Decomposition in Massive Networks](http://ieeexplore.ieee.org/xpl/login.jsp?tp=&arnumber=5767911&url=http%3A%2F%2Fieeexplore.ieee.org%2Fxpls%2Fabs_all.jsp%3Farnumber%3D5767911):
This paper attempts to utilize the external memory (disk) for graphs that do not fit in the main memory. While this could an appropriate solution for our case, it would be best to maintain everything in memory.
* [ParK: An efficient algorithm for k-core decomposition on multicore processors](http://ieeexplore.ieee.org/xpl/login.jsp?tp=&arnumber=7004366&url=http%3A%2F%2Fieeexplore.ieee.org%2Fxpls%2Fabs_all.jsp%3Farnumber%3D7004366):
The algorithm in this work utilizes a multi core approach and it is most likely the most efficient implementation of the k-core algorithm in a multi core environment. Moreover it seems to be capable of handling
large graph in aspects of edges and vertices. *Unfortunately, the paper only describes the algorithm without an implementation provided and - given the time frame- a suitable evaluation was not inclined for the time beeing.*  
-----------------------
##From A Paper graph to an Author Graph
On this section we describe the code and process for accessing the data and converting them to an author-citation graph.

The list of authors resides in a mongodb database where each author is stored along with a list of publications (stored as an ID of the publication). The publications themselves are also stored
in the same database. Their citation (or at least a clean and reliable version of them) are stored  in an SSDB database.

In order to get the  desired graph, we need to join the data from the mongodb and the SSDB. An initial option would be to do the join in one script/program that connects 
to both databases and:
1. retrieves authors and papers from mongodb
2. retrieves the relevant information from SSDB

This could be done in parallel with multiple threads but it would retrieve the same citations multiple times (as the size of the data do not allow to keep the entire citation database in memory).
Moreover, from some initial steps, it appears that **the current storage engine for the data does not allow concurrent access on mongodb (parallelCollectionScan) and multiple threads accessing the same 
database cursor seemed to fail**.

Thus we dump the respective data from both databases and join them in batches afterwards.

1. For mongodb we use `dump_authors.js` (located in the javascript folder). `mongodb <parameters> dump_authors.js >  authors.txt`. Saves in tab separated file authorid and publication ids
per line.
2. For SSDB we use `parallel_dumpCITES.py` (located in the python folder): It reads the _authors.txt_ file from the previous step in parallel. Each thread retrieves the cites for each publication -from the previous step-
and dumps in a tab separated file `<publication id>\t<tab separated citations>`. Each thread writes to a separate file `citations_<thread_id>.txt`.

###Join the Two Dumps

**First**, as it will be easier for the next steps to save the order of the Authors' Ids we need to run `convertAuthIdsToIncremental.py`. The reason why we need the order of the authors, is that
the D-Core implementation requires for efficiency to have the author's node at a specific position in array lists so that it would be faster to access it's information (edges, degree).
Thusly, this python script generates two files :

1. `authorsIncrementalIds.txt` : contains the mapping : position - <mongo id>.
2. `authorsPublIncrId.txt` : publications of each Author by incremental id.

**Afterwards**, we run the script `createGraph.py` that:

1. Groups authors by publications (using the incremental id)
2. Reads the files generated by the multiple threads of the `parallel_dumpCITES.py` one by one. For the cites in these files it adds (using in a python dictionary for the authors)
a new a edge among the pairs of authors between citer-cited papers. It saves the dictionary as a tab separated file `graphRESULT.txt` where the first column is the incremental author-id 
and the rest a list of other authors (that he cited).

###Reverse graph
As the D-Core algorithm maintains two threshold for each core (in and out degree) it would much more efficient to have both versions of a graph:

1. Outgoing edges: `graphRESULT.txt`
2. Incoming edges: `graphRESULTR.txt` . This one is generated by the python script `reverseGraph.py` and it has the same format as the outgoing edges file.

###Convert Graph to Java Object
The D-Core algorithm is implement in Java and loading such big data incrementally in Java is prohibitively slow as even the most efficient and pure java methods still create many small objects and causes a bottle neck on the Garbage Collector.  
As we will see later, we need to run D-cores in parallel and this cost should be avoid. It is much more efficient to convert *once**  the text files into the data objects that we will use  in the
D-core java program. We do this with the  `convertToObj.java` Java code. This requires also a file with  the degrees of each node in order to assist with a more efficient preallocation of resources.
Thusly:

1. We run first `countDegrees.py` to generate the degrees of each node in the file `degreesOUTIN.txt` (_tab separated <out-degree\tin-degree>_)
2. We run 
	+ `java convertToObj graphRESULT.txt 0` (the '0' means use the out degree - since we are reading the out-going edges) :  creates File `graphRESULT.txtobj`
	+ `java convertToObj graphRESULTR.txt 1` (the '1' means use the in degree - since we are reading the in-coming edges) :  creates File `graphRESULTR.txtobj`

The txt files now are not useful and can be removed if require the space.

-----------------------

 
##D-CORE: Implementation, Issues & Other notes


Even the most efficient k-core implementation starts to become "cumbersome"  when we transfer it to the  directed graphs. This is because monotonicity between core partitions can be guaranteed only
in one direction at a time. To give a simple example D-core(3,3) can result from D-core(2,2) either through D-core(2,3) or D-core(3,2) *AND* D-core(2,3) cannot result from D-core(3,2) (or vice versa).

In practice this means that we can compute D-core partitions along one direction in parallel and we can use the previous cores from the same direction to compute the next sequentially.
For example, we can have all the D-core decompositions along the out-degree direction; sequentially for one in-degree and in parallel for all in-degrees.
Pseudocode:

    function d-core(i):\n
        For j in (all out-degree thresholds):
    Compute d-core(i,j) 
        Assume in memory object the uses d-core(i,j) information 
        to compute d-core(i,j+1) 
    For i in (all in-degree thresholds) compute in parallel:
        d-core(i)
		
To keep the computation of the algorithm as efficient as possible we maintain the graph unchanged in memory and we compute only the changes in the degrees as we remove nodes.
*This is similar to the tactic as Batagelj; the main difference is that we don not keep an order of the edges .* For on single "direction" e.g. if we want to compute the decomposition
along one value of an in-degree, the computation is as efficient as the algorithms mentioned above (or at least very close) i.e. the computation is in the magnitude of 1 hour.
This would be completely acceptable time-cost of execution if it was a k-core decomposition.

The issues arise with the fact that we need to compute this for multiple values of (lets say ) the in-degree. We can save partial results and reload them when needed as cache.
We must not forget thought the size of the graph in disk and in memory. 

**Notes:** 

+ _At the time of writing, the available "free" resources were at 400GB of RAM and ~250GB of disk_
	+ We are saving only the node ids (the incremental ones) for each D-core(i,j) in txt format in gziped file for further analysis later on (**Folder `cores_ids` needs to be created manually**).
	+ At the first steps -when we are loading the entire graph- we avoid multiple instances (or at least limit them to very few)- as the memory consumption is massive.
+ The java Garbage Collector becomes too slow for very large structures that stay very long in memory (i.e. the graph).
+ Multiprocessing would require a complete copy of all structures or a somewhat slower (and more time consuming to implement) shared structure.

The implementation **FOR NOW** includes an ad hoc approach of caching some results, filtered versions of the graph based on those caches and initializing multiple instances of the 
core decomposition program for the ranges we desire *based on our own judgment*

**Files:** 

+ `lastCores.java` : computes the D-core(0,J) and D-core(J,0) (I & J: for all possible i,j until we have an empty graph). These have to be computed separately as we can use information from one to compute the other.
	+ It also computes the cache of nodes ids along D-core(i,1) and the corresponding decompositions. These cache files are loaded automatically later on when we launch different decompositions.
		So instead of re-scanning all nodes we use the cache file to get a pre-filtered list of nodes to scan.
	+ ** Folder : `cache` needs to be created manually**
+ `filterGraphCache.java` : filters out information from the graph objects (graphRESULT and graphRESULTR) by using the information from the files in the cache folder.
	We save these filtered out graph object  as new file objects that can be used to load the filtered out graph (instead of the original). This way, for the subsequent D-cores, we
	load the filtered graph faster and we ask from the JVM less memory.
	+ For now it is up to own judgments when to produce a new filtered version of the graph (based on the available disk space and how much memory the current graph consumes in the 
	main memory)
+ `lastCores2.java` : computes the decomposition from D-core(i,J) to D-core(i',J) (i,i' are given as an input - i<i' -) by utilizing a filtered graph (that we also specify as input).

*Reminder: The graph is composed by two object: OUT and IN edges*

**Usage:**
*First*, we need to run `lastCores`:

	java -Xmx200G -Xms100G  -XX:ParallelGCThreads=25 -XX:ConcGCreads=6  lastCores
	
The java parameters are essential for the specific dataset.

Then, we move along the in-axis. Lets say we want to initialize in parallel :
* D-core(500,J) to D-core(550,J)
* D-core(550,J) to D-core(600,J)
* D-core(600,J) to D-core(650,J)

**AND we want to utilize the a filtered graph that corresponds to the cache D-core(500,1)** (we could use any number lower than 500):

		java -Xmx40G -Xms30G  -XX:ParallelGCThreads=25 -XX:ConcGCThreads=6  filterGraphCache graphRESULT 500 //filter the out-going edges
		java -Xmx40G -Xms30G  -XX:ParallelGCThreads=25 -XX:ConcGCThreads=6  filterGraphCache graphRESULTR 500 //filter the in-coming edges
*We can launch the two commands above in parallel.*
After we start manually in parallel:
	
		java -Xmx40G -Xms30G  -XX:ParallelGCThreads=25 -XX:ConcGCThreads=6  lastCores2 500 550 500
		java -Xmx40G -Xms30G  -XX:ParallelGCThreads=25 -XX:ConcGCThreads=6  lastCores2 550 600 500
		java -Xmx40G -Xms30G  -XX:ParallelGCThreads=25 -XX:ConcGCThreads=6  lastCores2 600 650 500
As you may notice we need less memory for the JVM now.

-----------------------

##Parsing the Results:
As the decomposition might take some time to complete for all possible values, we can get partial results (and combine them later).
We use for this the `partialFrontiers.py` script.
**NOTE:**
We need to set in the script:

* the range of inDegrees : `fromin` and `toin`
* the maximum out degree to search for : `outT` . **This variable appears 3 times. We need to set it all three times** 
* a file counter `result_count` so that the output filename will change each time we run the script and won't overwrite past partial results.

This script generates two files:

+ A file with the size of each partition in the given range `all_front_%d.txt` : line format `<in-degree>\t<out-degree>\t<D-core(in,out) partition size>`
+ A file with the maximum D-core on the direction of the out-degree per in-degree for the given range `authFront_%d.txt`: line format `<node id>\t<tab separated pairs: in#max-out>`

##In order to produce the final data that will uploaded in MongoDB:

We need to run the following files in the this specific order:

1. `parseALLAUTHORS.py` : Will create :
	+ a file `dataGRAPH.json` that contains the data for the entire graph (maximum frontier, volume and angle). 
	+ a file `joined.txt` that contains the merging of the previous `authFront_%d.txt` files. 
	**It requires the files from the previous step**:
	+`all_front_%d.txt` : all of these that were generated in the previous step have to be set as a list of strings in the first* occurrence of the `files` variable.
	+ `authFront_%d.txt` : all of these that were generated in the previous step have to be set as a list of strings in the *second* occurrence of the `files` variable.
	It also reads the file `authorsIncrementalIds.txt` to replace the Incremental Ids with the ones used in Mongodb
2. `parallelJSON.py` : Will create multiple json files `authorDATA_<#>.json` that contain parts of the collection of authors that are *not only* in the D-Core(0,0).
	It reads the `joined.txt` file in parallel. *We also set in this one manualy (as it has already been computed) the variables 
	+ volumeALL : total volume of the graph 
	+ maxOUTALL : max out degree of the D-core decomposition
	+ maxINALL : max in degree of the D-core decomposition
3. `noFRONTIER.py` : Will create a file `dataDCORES_0_0.json` with the authors that are *only* in in the D-Core(0,0). **It needs the same file requirements as the script  `parseALLAUTHORS.py`***.
-----------------------
##**Plot of D-Core partition sizes:**

![D-CORE Sizes](https://bytebucket.org/xristosakamad/aminer_dcores/raw/a352c1de596672cbcb753d3bc688165e798f37ab/images/d-core.png)

###**Evaluating authors based on D-cores:**

We can track an author along his maximum D-core frontier and we can plot that frontier.
Moreover we can compute :
	
* the normalized area/volume under that frontier (normalized by the total one of the entire graph).
* the angle of the bounding box of the frontier (and normalize it as well in respect to the entire graph).
	
This produces two metrics that characterize the frontier. 
For an compact and intuitive presentation of these two metrics we can display the volume as a number and the angle as a color.
We can see a demo of this concept **[HERE](http://moodle.lix.polytechnique.fr/dcore_demo)**


-----------------------

##Future Work and TODOS:

1. **DW-CORES** : While there can be done some optimizations (described bellow) for automatically caching and setting up parallel decompositions, **the most interesting** future task would be to 
	extend the D-CORE structure into weighted graphs. This is would provide greater detail on the connections among authors:
	
	* At the moment the directed graph treats equally one citation and multiple ones
	* The importance of the cited paper and the citing paper are not taken into account
	* The number of authors is not taken into account neither the number of citations in the paper
	
	All these aspects can be used to better evaluate the strength of the connections among papers and their authors. 
	Moreover it would provide a better refinement and a fairer evaluation methodology and metric for the authors.
	
2. As mentioned above this decomposition can be better optimized with an automated protocol of caching. 
Moreover as Java Memory Management seems to perform rather badly in very large structures, it would be best to re-implement the D-core algorithm in another language so that:
	* we would avoid the manual setting of memory parameters that depend on the expected size of the graph
		+ this way we can share objects as they decrease in size and not re-launch the program
	* the protocol would describe an automated manner to produce the cache and use it to filter the graph as we move along the decomposition (in parallel to the main execution)
		+ as we filter the graph we can automatically increase the number of threads (as more of them will fit in memory)
	
3. Finally, a **streaming adaptation of the D-CORE** (and subsequently the future DW-CORE) should be constructed. There is already work on how to update  the k-core structure : 
[Streaming Algorithms for k-core Decomposition](http://systemg.research.ibm.com/papers/T2.1-streaming-algorithms-for-k-core-decomposition.pdf). The theorems of this work can be extended
to the directed and weighted graphs in order to provide an updating framework for the already computed structures. With these extensions we will need to compute the decompositions once  and
then add incremental updates in regular periods (instead of recomputing the entire decomposition).

