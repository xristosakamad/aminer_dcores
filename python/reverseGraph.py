

reverseGraph={}

f=open('./graphRESULT.txt','r')
maxV=0
c=0
for line in f:
	if(c%50000==0): print c
	t=line.strip().split('\t')
	for v in t:
		if v not in reverseGraph:
			reverseGraph[v]=[]
			maxV=max(maxV,int(v))
	for v in t[1:]:
		reverseGraph[v].append(t[0])
	c+=1
	
f.close()

f=open('./graphRESULTR.txt','w')

for i in range(0,maxV+1):
	f.write('%d\t%s\n'%(i,('\t'.join(reverseGraph[str(i)]))))

f.close()