
authorMongIDS=[]
authorPos={}
authorPubs=[]
f=open('./authors.txt','r')
#skip first two lines
f.readline()
f.readline()
for line in f:
	t=line.strip().split('\t')
	authorPos[t[0]]=len(authorMongIDS)
	authorMongIDS.append(t[0])
	authorPubs.append(t[2:])
f.close()

f=open('./authorsIncrementalIds.txt','w')
for v in authorMongIDS:
	f.write('%d\t%s\n'%(authorPos[v],v))
f.close()

f=open('./authorsPublIncrId.txt','w')
for v in authorMongIDS:
	f.write('%d\t%s\n'%(authorPos[v],('\t'.join(authorPubs[authorPos[v]]))))
f.close()