
import numpy as np
import math
import operator
import json

maxOUTALL=0
maxINALL=0
volumeALL=0
frontierALL=[]

dataJSON=[]
sizes={}
#rel.insert({"special":"ALL_ACM","f":frontier_all,"mi":maxIn,"mo":maxOut,"vol":volumeall})
files=["all_front.txt","all_front_2.txt","all_front_3.txt","all_front_4.txt","all_front_5_6_fixed.txt","all_front_7.txt","all_front_8.txt","all_front_9.txt","all_front_10.txt"]
for file in files:
	f=open(file,'r')
	for line in f:
		
		t=[int(x) for x in line.strip().split('\t')]
		#print t
		sizes[(t[0],t[1])]=t[2]
		maxINALL=max(t[0],maxINALL)
		maxOUTALL=max(t[1],maxOUTALL)
	f.close()
	

angleALL=maxOUTALL/math.sqrt(maxOUTALL*maxOUTALL+maxINALL*maxINALL);	
data=np.zeros((maxINALL+1,maxOUTALL+1))

for key in sizes:
	data[key[0],key[1]]=sizes[key]
for i in range(0,maxINALL+1):
	for j in range(maxOUTALL,-1,-1):
		if(data[i,j]!=0):
			frontierALL.append([i,j])
			volumeALL+=j
			break
print "volumeALL:%d"%volumeALL
########################3
dataJSON.append({"special":"ALL_DATA","f":frontierALL,"mi":maxINALL,"mo":maxOUTALL,"vol":volumeALL,"ang":angleALL})
json.dump(dataJSON, open("./dataGRAPH.json",'w'))
#########################
print "loadding realIDS"
realIDS=[]

print "load real authorIDS"
f=open("authorsIncrementalIds.txt",'r')
for line in f:
	t=line.strip().split("\t")
	realIDS.append(t[1])
f.close()

print "Starting authors: join"

files=["authFront_2.txt","authFront_3.txt","authFront_4.txt","authFront_5.txt","authFront_6.txt","authFront_7.txt","authFront_8.txt","authFront_9.txt","authFront_10.txt"]

joinDATA={}
for file in files:
	print file
	f=open(file,'r')
	for line in f:
		t=line.strip().split("\t")
		if(t[0] not in joinDATA):
			joinDATA[t[0]]=""
		if(joinDATA[t[0]]==""):
			joinDATA[t[0]]="\t".join(t[1:])
		else:
			joinDATA[t[0]]=('%s\t%s'%(joinDATA[t[0]],("\t".join(t[1:])))).strip()
	f.close()
count=0	
fw = open("joined.txt",'w')
for keyA in joinDATA:
	if(count%10000==0): print '\t%d'%count
	fw.write("%s\t%s\n"%(realIDS[int(keyA)],joinDATA[keyA]))
	count+=1
fw.close()	



	
