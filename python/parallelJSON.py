from multiprocessing import Process, Manager,Pool
import multiprocessing
import sys
import time
import threading
import os
import numpy as np
import math
import operator
import json


queueLock =threading.Lock()

def readB(f,CR,END, chunk=1024):
	pre=""
	c=False
	chunk= chunk if CR+chunk<END else END-CR
	f.seek(CR)
	x=f.read(chunk)
	#print 'CR:'+str(CR)+' END:'+str(END)
	c=0
	while x:
		x="".join((pre,x))
		#print 'x:\n'+x+'\nxxx'
		pre=""
		lines=x.split("\n");
		leng=len(lines)-1
		for i in range(leng) :
			#c=c+1
			yield lines[i]#str(c)+"::"+
		pre=lines[leng]
		CR=CR+chunk
		#print 'CR:'+str(CR)
		if(CR>=END): break
		chunk= chunk if CR+chunk<END else END-CR
		x=f.read(chunk)
	yield pre
def findN(f,CR) :
	f.seek(CR)
	x=f.read(1)
	while x :
		CR=CR+1
		if(x=='\n') :
			#print '\tfound'+str(CR)
			return CR
		x=f.read(1)
	#print '\tfound'+str(CR)
	return CR


class myThreadT (Process):
    def __init__(self, threadID,seek,end,path):
		Process.__init__(self)
		#super(myThreadT, self).__init__()
		#threading.Thread.__init__(self)
		self.threadID = threadID
		self.seek=seek
		self.end=end
		self.path=path
		
			
		
    def run(self):
		#I already know these...no need to loadthem
		volumeALL=1609547.0
		maxOUTALL=1354.0
		maxINALL=1231.0
		###
		global queueLock
		f=open(self.path,'rb')
		fw=open('./authorDATA_%d.json'%self.threadID,'w')
		dataJSON=[]
		count=0
		for line  in readB(f,self.seek,self.end):
			spt=line.strip().split("\t")
		#for keyA in joinDATA:
			#print [[int(x[0]), int(x[1])] for x in [t.split('#') for t in joinDATA[keyA].split("\t") ]]
			temp=[[int(x[0]), int(x[1])] for x in [t.split('#') for t in spt[1:] ]]
			sortedF=sorted(temp,key=operator.itemgetter(0,1))
			if(count%10000==0): print '\t %d: %d'%(self.threadID,count)
			
			tmpMaxIn=0
			tmpMaxOut=0
			tmpVolume=0
			tmpAngle=0
			tmpVolumeN=0
			for row in sortedF:
				tmpMaxIn=max(tmpMaxIn,row[0])
				tmpMaxOut=max(tmpMaxOut,row[1])
				tmpVolume+=row[1]
			if tmpMaxIn==0:
				tmpVolume=tmpMaxOut
			if tmpMaxOut==0:
				tmpVolume=tmpMaxIn
			if(tmpMaxIn==0 and tmpMaxOut==0):
				tmpAngle=-666
			else:
				tmpAngle=tmpMaxOut/math.sqrt(tmpMaxOut*tmpMaxOut+tmpMaxIn*tmpMaxIn);	
			tmpVolumeN=tmpVolume/volumeALL
			dataJSON.append({"_id":{"$oid":spt[0]},
							"f":sortedF,
							"mi":tmpMaxIn,"mo":tmpMaxOut,"vol":tmpVolume,
							"volN":tmpVolumeN,"angl":tmpAngle})
			count+=1
		print "Dumping"		
		json.dump(dataJSON, fw)
		f.close()
		fw.close()
		
if __name__ == "__main__":
	path='./joined.txt'
	f=open(path,'rb')
	sz=os.path.getsize(path)
	starts=[findN(f,1)]
	for i in range(1,20):
		starts.append(findN(f,i*sz/20))
	ends=[]
	for i in range(1,20):
		ends.append(findN(f,i*sz/20))
	ends.append(sz+1)
	f.close()
	threads = []
	threadID = 1
	threadList =["Thread-%d"%x for x in range(1,21) ]
	# Create new threads
	for i,tName in enumerate(threadList):
		thread = myThreadT(threadID,starts[i],ends[i],path)
		thread.start()
		threads.append(thread)
		threadID += 1
	start=time.time()	
	# Wait for all threads to complete
	for t in threads:
		t.join()
	print "Exiting Main Thread"
	end=time.time()
	print str((end-start))+" s"
		