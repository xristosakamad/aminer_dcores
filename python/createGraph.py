from multiprocessing import Pool
from functools import partial




if __name__ == '__main__':
	publicationDict={}
	authorCiteSets=[]
	print "Group authors by Publication AND load authorIncIds" 
	c=0
	f=open('./authorsPublIncrId.txt','r')
	for line in f:
		if(c%50000==0) : print '\t%d'%c
		t=line.strip().split('\t')
		for publ in t[1:]:
			if publ not in publicationDict:
				publicationDict[publ]=[]
			publicationDict[publ].append(int(t[0]))
		authorCiteSets.append(set())
		
		c+=1
	f.close()
	
	print "Starting Graph building"
	for i in range(1,16):
		print 'File: %d'%i
		citationDict={}
		c=0
		f=open('./dumpC/citations_%d.txt'%i,'r')
		for line in f:
			if(c%50000==0): print '\t%d: %d'%(i,c)
			t=line.strip().split('\t')
			citationDict[t[0]]=t[1:]
			c+=1
		f.close()
		print 'Dict: %d'%i
		c=0
		#for each publication:key->cited paper:citeP create a link among all pairs between citer(s) cA and cited cB
		for key in citationDict:
			if(c%50000==0): print '\t D: %d: %d'%(i,c)
			if key in publicationDict:
				for  citedP in citationDict[key]:
					if citedP in publicationDict:
						for cA in publicationDict[key]:
							for cB in publicationDict[citedP]:
								if not cA==cB: authorCiteSets[cA].add(cB)
			c+=1
	print "Saving"
	f=open('./graphRESULT.txt','w')
	for i,ss in enumerate(authorCiteSets):
		#citer\t<tab separated cited>
		f.write('%d\t%s\n'%(i,('\t'.join([str(tp) for tp in ss]))))
	f.close()