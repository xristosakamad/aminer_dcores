
import numpy as np
import math
import operator
import json

print "loadding realIDS"
realIDS={}
trueKEY={}


print "load real authorIDS"
f=open("authorsIncrementalIds.txt",'r')
for line in f:
	t=line.strip().split("\t")
	realIDS[t[0]]=False
	trueKEY[t[0]]=t[1]
f.close()

files=["authFront_2.txt","authFront_3.txt","authFront_4.txt","authFront_5.txt","authFront_6.txt","authFront_7.txt","authFront_8.txt","authFront_9.txt","authFront_10.txt"]


for file in files:
	print file
	f=open(file,'r')
	for line in f:
		t=line.strip().split("\t")
		realIDS[t[0]]=True
	f.close()
dataJSON=[]


print "DUMPING 0 0"
count=0
fw=open("./dataDCORES_0_0.json",'w')
fw.write("[")
for key in realIDS:
	if not realIDS[key] :
		if(count%100000==0): print count
		if(count==0) : fw.write('%s'%json.dumps({"_id":{"$oid":trueKEY[key]},"f":[],"mi":0,"mo":0,"vol":0,"volN":0,"angl":-666}))
		else : fw.write(',\n%s'%json.dumps({"_id":{"$oid":trueKEY[key]},"f":[],"mi":0,"mo":0,"vol":0,"volN":0,"angl":-666}))
		#dataJSON.append()
		count+=1
fw.write("]")
fw.close()
print "HERE:%d"%count


