import os.path as op
import gzip
from multiprocessing import Pool
def getSizes(i):
	outT=1354
	tempFront=[]
	print i
	for outC in range(outT,-1,-1):
			path="./cores_ids/core_%d_%d.txt.gz"%(i,outC)
			#print path
			if not(i==0 and outC==0):
				
				if op.exists(path):
					#print inC,outC-2
					size=0
					with gzip.open(path,'r') as fr:
						for frline in fr:
							size+=1
					tempFront.append((i,outC,size))
				
	return tempFront
	
def parFront(i):
	print "\t%d"%i
	outT=1354
	temp=set()
	tempAFront={}
	print "\t%d"%i
	for j in range(outT,-1,-1):
		
		
		path="./cores_ids/core_%d_%d.txt.gz"%(i,j)
		#print "\t%s"%path
		if op.exists(path):
			with gzip.open(path,'r') as fr:
				for frline in fr:
					authid=frline.strip()
					if authid not in tempAFront:
						tempAFront[authid]=[]
					if authid not in temp:
						temp.add(authid)
						tempAFront[authid].append((i,j))

	return tempAFront

if __name__ == '__main__' :
	outT=1354
	fromin=0
	toin=200
	result_count=0
	pool=Pool(10)
	frontier_all=[]

	frontier_all.extend([ item for sublist in pool.map(getSizes,range(fromin,toin+1)) for item in sublist ])
	
	
	print "frontier_all"
	fw=open('all_front_%d.txt'%result_count,'w'); #5,4
	for row in frontier_all:
		fw.write('%d\t%d\t%d\n'%row)
	fw.close()	



	print "starting frontier calc"
	authorfrontiers={}


	
	
	frontier_pA=pool.map(parFront,range(fromin,toin+1))
	
	
	for tempAFront in frontier_pA:
		for tkey in tempAFront:
			if tkey not in authorfrontiers:
				authorfrontiers[tkey]=tempAFront[tkey]
			else:
				authorfrontiers[tkey].extend(tempAFront[tkey])
		
				
	print "saving partial fronties"		
	fw=open('authFront_%d.txt'%result_count,'w');
	for authid in authorfrontiers:
		fw.write('%s\t%s\n'%(authid,('\t'.join(['%d#%d'%(x[0],x[1]) for x in authorfrontiers[authid]]))))

	fw.close()
	

