from multiprocessing import Process, Manager,Pool
import multiprocessing
import sys
import time
import threading
import os
from ssdb import SSDB

queueLock =threading.Lock()
#funtion to read file from CR to END
def readB(f,CR,END, chunk=1024):
	pre=""
	c=False
	chunk= chunk if CR+chunk<END else END-CR
	f.seek(CR)
	x=f.read(chunk)
	#print 'CR:'+str(CR)+' END:'+str(END)
	c=0
	while x:
		x="".join((pre,x))
		#print 'x:\n'+x+'\nxxx'
		pre=""
		lines=x.split("\n");
		leng=len(lines)-1
		for i in range(leng) :
			#c=c+1
			yield lines[i]#str(c)+"::"+
		pre=lines[leng]
		CR=CR+chunk
		#print 'CR:'+str(CR)
		if(CR>=END): break
		chunk= chunk if CR+chunk<END else END-CR
		x=f.read(chunk)
	yield pre
#function to find the closest newline character given an offset byte CR
def findN(f,CR) :
	f.seek(CR)
	x=f.read(1)
	while x :
		CR=CR+1
		if(x=='\n') :
			#print '\tfound'+str(CR)
			return CR
		x=f.read(1)
	#print '\tfound'+str(CR)
	return CR
#assuming tab separate file and that column 2 and afterwards has list of paper ids
#thread that reads a file from seek to end and retrieces the cites for each paper found
#then save the cites for each paper as one line : paperid\t<tab separated list of cites>
class myThreadT (Process):
    def __init__(self, threadID,seek,end,path):
		Process.__init__(self)
		ssdb=SSDB(host='166.111.7.173', port=12003)
		#super(myThreadT, self).__init__()
		#threading.Thread.__init__(self)
		self.threadID = threadID
		self.seek=seek
		self.end=end
		self.path=path
		
			
		
    def run(self):
		global queueLock
		f=open(self.path,'rb')
		fw=open('./citations_%d.txt'%self.threadID,'w')
		sdb=SSDB(host='166.111.7.173', port=12003)
		for line  in readB(f,self.seek,self.end):
			line=line.strip();
			t=line.split('\t')
			if len(t)>=2:
				tcites=[x for x in sdb.zrange('PUB::REL::CITE::C::%s'%t[0],0,100)]
				#paperid\t<tab separated list of cites>
				fw.write('%s\t%s\n'%(t[0],('\t'.join(tcites))))
		f.close()
		fw.close()
		
if __name__ == "__main__":
	path='./pubs.txt'
	f=open(path,'rb')
	sz=os.path.getsize(path)
	starts=[findN(f,1)]
	#15 threads
	for i in range(1,15):
		starts.append(findN(f,i*sz/15))
	ends=[]
	for i in range(1,15):
		ends.append(findN(f,i*sz/10))
	
	ends.append(sz+1)
	f.close()
	threads = []
	threadID = 1
	threadList =["Thread-%d"%x for x in range(1,16) ] 
	# Create new threads
	for i,tName in enumerate(threadList):
		thread = myThreadT(threadID,starts[i],ends[i],path)
		thread.start()
		threads.append(thread)
		threadID += 1
	start=time.time()	
	# Wait for all threads to complete
	for t in threads:
		t.join()
	print "Exiting Main Thread"
	end=time.time()
	print str((end-start))+" s"
		